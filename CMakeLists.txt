
cmake_minimum_required(VERSION 3.20)

project(articles VERSION 0.2 LANGUAGES NONE)

# Each variable set before the addition of a subdirectory is available to the
# CMakeLists.txt in that subdirectory.

# This is supplied on the command-line either
#
# - to latexml (via `--javascript=${MATHJAX}`) or
# - to pandoc  (via `--mathjax=${MATHJAX}`)
#
# if the document have mathematical content.
set(MATHJAX "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js")

# Lua-function that, at the end of a reference's name in a markdown-file,
# transforms `.md` into `.html`.  This allows an index-file to refer to
# another index-file or to an article's source in the repository, and each
# file built by pandoc on github-pages will still refer to the right built
# file.
set(MD_TO_HTML "${CMAKE_CURRENT_SOURCE_DIR}/md-to-html.lua")

# Lua-function that, at the end of a reference's name in a markdown-file,
# transforms `.tex` into `.html`.  This allows an index-file to refer to
# another index-file or to an article's source in the repository, and each
# file built by pandoc on github-pages will still refer to the right built
# file.
set(TEX_TO_HTML "${CMAKE_CURRENT_SOURCE_DIR}/tex-to-html.lua")

# Path to template used for pandoc's md -> html.
set(PANDOC_TEMPLATE_HTML "${CMAKE_CURRENT_SOURCE_DIR}/pandoc-template.html")

set(PANDOC_HTML pandoc
  -s
  --lua-filter=${MD_TO_HTML}
  --lua-filter=${TEX_TO_HTML}
  --template=${PANDOC_TEMPLATE_HTML}
  --mathjax=${MATHJAX}
)

set(PANDOC_PDF pandoc
  -s
  --lua-filter=${MD_TO_HTML}
  --lua-filter=${TEX_TO_HTML}
)


add_custom_target(cleaner
  COMMAND ${CMAKE_COMMAND} -E remove_directory  .cmake
  COMMAND ${CMAKE_COMMAND} -E remove            CMakeCache.txt
  COMMAND ${CMAKE_COMMAND} -E remove_directory  CMakeFiles
  COMMAND ${CMAKE_COMMAND} -E remove            Makefile
  COMMAND ${CMAKE_COMMAND} -E remove            cmake_install.cmake
  COMMENT "Cleaning up top-level cmake-files."
)

add_subdirectory(public)

# Cause target (accidentally having same name as subdirectory) to be built.
add_custom_target(articles DEPENDS public)

# Make the default (all) target depend on the articles-target.
set_target_properties(articles PROPERTIES
  EXCLUDE_FROM_ALL FALSE
  EXCLUDE_FROM_DEFAULT_BUILD FALSE
)

# EOF
