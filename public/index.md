---
title: "Articles"
author: "Thomas E. Vaughan"
description: Some articles by Thomas E. Vaughan.
image: https://tevaughan.gitlab.io/articles/sample-zoom.png
url: https://tevaughan.gitlab.io/articles/index.html
---

## List of Articles

- [Pandoc-Markdown Example](pandoc-example/)
  ([HTML](pandoc-example/article.html), [PDF](pandoc-example/article.pdf))

- [The Horizon of a Spherical Earth](horizon/)
  ([HTML](horizon/article.html), [PDF](horizon/article.pdf))

## Views of This Site

If you are viewing the present page on the internet, you are viewing one or
the other of two, corresponding sites.

1. [source-code site]
2. [web-rendering site]

[source-code site]: https://gitlab.com/tevaughan/articles
[web-rendering site]: https://tevaughan.gitlab.io/articles

The PDF-version of each article is accessible via either site.

On the source-code site, any link to an HTML-page will show the HTML-source
for that page.

<!-- EOF -->
