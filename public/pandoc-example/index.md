---
author: Thomas E. Vaughan
description: Toy-Article written in pandoc-markdown.
title: Example of Article With Pandoc-Markdown Source
url: https://tevaughan.gitlab.io/articles/pandoc-example/index.html
---

- [Up](../index.md).

- An article ([HTML](article.html), [PDF](article.pdf)) written in
  pandoc-markdown.
  - It has a figure written in latex/tikz as a separate file.
  - The figure appears in each of the HTML and PDF versions.
  - The figure's caption, which is written into the optional text for the
    figure's inline-image reference in the markdown, is prefixed with "Figure
    1" in the PDF.

<!-- EOF -->
