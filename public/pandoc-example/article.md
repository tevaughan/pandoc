
---
title: Article Serving as Pandoc-Markdown Example
author: Thomas E. Vaughan
abstract: |
  This article was written in pandoc-markdown.  It has a figure, whose source
  is a separate latex/tikz file.
...

- [Up](../index.md).

## Introduction

Here is the figure:

![Location and appearance of horizon.](figure-1.pdf)

## Another Section

Here is some mathematics:

\begin{eqnarray*}
r^2 + d^2 &=& [r + a]^2\\
      d^2 &=& 2ra + a^2\\
      d   &=& \sqrt{a[2r + a]}
\end{eqnarray*}

