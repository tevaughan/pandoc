\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{caption}   % \captionsetup
\usepackage{fancyhdr}  % fancy pagestyle
\usepackage{graphicx}  % \includegraphics
\usepackage{listings}  % lstlisting-environment

\usepackage{hyperref}  % \href

\captionsetup[figure]{font=sf}

\lstset{
  basicstyle=\ttfamily,
  columns=fullflexible,
}

\setlength{\headheight}{14.5pt}
\addtolength{\topmargin}{-2.5pt}
\pagestyle{fancy}
\fancyfoot[L]{Copyright 2024 Thomas E. Vaughan}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}

\title{The Horizon of a Spherical Earth}
\author{Thomas E. Vaughan}

\begin{document}

\maketitle

\begin{center}
Copyright 2024 Thomas E. Vaughan
\end{center}

\abstract{
  A model of the Earth, as a perfect sphere with radius $r = 3960$~mi and
  with no surrounding atmosphere, predicts that a cell-phone camera, with a
  focal length of 3000 pixels and at an altitude of 35,000~ft, will see
  10.6~pixels (0.2~deg) of curvature in the horizon over an angular baseline
  on 39.1~deg.
  %
  The horizon is predicted to be 3.31~deg farther from the zenith than the
  horizon of a flat-Earth model of large extent.
  %
  The measured curvature is about 10~pixels, consistent with the prediction.

  Suppose that a camera is situated at altitude $h$ and oriented so that the
  horizon crosses the field of view.
  %
  The model predicts that the depression of the horizon away from the zenith,
  relative to the horizon if the Earth were a very wide plane, is
  \begin{equation*}
    \delta = \arccos\frac{r}{r + h} \simeq \sqrt{\frac{2h}{r}},
  \end{equation*}
  where the deviation of the approximation from the exact prediction of the
  model is smaller than 1\% for $h < 160$~mi.
  %
  Suppose that, within the field of view, $\varphi$ is the largest angle of
  separation between two image-points, $L$ and $R,$ both on the horizon and
  each at the same angular separation from the center $K$ of the image.
  %
  Let $P$ be the image-point on the horizon closest to $K,$ and let the
  angular separation between $P$ and the midpoint $M$ of the line-segment
  between $L$ and $R$ be $\varepsilon$.
  %
  Then the angle $\varepsilon$ is a measure of the horizon's apparent
  curvature over the angular baseline $\varphi$.
  %
  The model predicts that
  \begin{equation*}
    \varepsilon \simeq
    \left[\sec\left(\frac{\varphi}{2}\right) - 1\right] \sqrt{\frac{2h}{r}},
  \end{equation*}
  where the expression is a value in radians.%
}

\section{Introduction}

I derive an expression for the predicted, apparent curvature of the horizon
as observed by a camera above a spherical model of the Earth.
%
Then I show an image, taken of the horizon from an airplane flying at roughly
35,000~ft; I obtained this image by searching the internet for pictures of
the horizon.
%
Finally, I compare the prediction of the theory against the image.

\section{Prediction}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]{diagram}
  \end{center}
  \caption{%
    Side-view (upper diagram) and top-view (lower diagram) of a
    camera's relationship to the horizon.
    %
    Camera at point $O$ is located at altitude $h$ above surface of sphere
    with radius $r$ and center $C.$
    %
    $P$ is point on horizon with direction closest to central direction in
    camera's field of view.
    %
    In lower diagram is view from above $O$ and toward $C$ (behind $O$).
    %
    Angle $\varphi$ is largest angular separation between points that are on
    horizon, in camera's field of view, and at equal angular separation from
    central direction in camera's field.
    %
    $M$ is midpoint of segment $LR$.
    %
    Small angular difference $\varepsilon$ between direction $OP$ and
    direction $OM$ is depicted (but not labeled) in upper diagram.%
  }
  \label{fig:diagrams}
\end{figure}

For a model in which the Earth has no atmosphere and is a sphere with radius
$r = 3960$~mi, the predicted distance $d$ to the horizon from an observer at
altitude $h$ above the surface obeys the equation $d^2 = 2rh + h^2$.
%
See the side-view (upper diagram) in Figure~\ref{fig:diagrams}.
%
The approximate prediction
\begin{equation}
  \begin{array}{ccrr}
    d &\simeq&        \sqrt{2rh}&\\
    &\simeq&   [90\:\text{mi}]&\sqrt{\frac{h}{1\:\text{mi}}}\\
    &\simeq& [1.22\:\text{mi}]&\sqrt{\frac{h}{1\:\text{ft}}}
  \end{array}
\end{equation}
has, relative the the exact prediction, an error smaller than one percent so
long as $h < 160$~mi.
%
In this model, an observer on the beach with eyes at $h = 6$~ft above the
level of the sea is predicted to see the horizon about three miles away; if
the sea were rough with twelve feet from trough to crest%
\footnote{%
  This is the definition of a 12-foot sea, which is the root-mean-square of
  the trough-to-crest height of swells (waves propagated from distant winds)
  and the height of local waves%
},
the observer would be able to see distant crests up to six miles away.
%
An observer on an airplane flying at $h = 6$~mi above the ocean is predicted
to see the horizon about 220~mi away in the model with no atmosphere, but the
horizon might appear 270~mi away, depending on atmospheric conditions,
because of atmospheric refraction as the index of refraction changes with
altitude.

Consider a camera that is located at altitude $h$.
%
Let the camera be oriented such that within the camera's field of view,
$\varphi$ is the angle separating the two most widely separated image-points
$L$ and $R$ on the horizon, such that each of $L$ and $R$ is the same angular
distance from the center $K$ of the image.
%
In the image-plane, let $P$ be the point on the horizon closest to $K,$ and
let $M$ be the midpoint of the segment $LR.$
%
The angular separation $\varepsilon = \alpha - \beta$ is the difference between
two angles.
%
The larger is the angle $\alpha = \arcsin{\frac{r}{r + h}}$ between the
nadir and the direction corresponding to $P.$
%
The smaller is the angle
\begin{equation}
  \beta =
    \frac{\pi}{2} -
    \arctan\sqrt{\frac{2rh + h^2}{r^2 - [r + h]^2\sin^2\frac{\varphi}{2}}}
\end{equation}
(here represented in radians) between the nadir and the direction
corresponding to $M.$
%
The difference $\varepsilon$ is a measure of the apparent curvature of the
horizon at altitude~$h$ and by way of an angular baseline $\varphi$.
%
If $h$ be small (smaller than 160~mi or so), the difference in radians is
predicted to be approximately
\begin{equation}
  \varepsilon \simeq \left[ \sec\frac{\varphi}{2} - 1 \right] \sqrt{\frac{2h}{r}}.
\end{equation}

\section{Observation}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{horizon-35k}
  \end{center}
  \caption{Image taken by Sébastien Nadeau from airplane at about 35,000~ft
  in altitutde.
    The full width of about 4032~pixels is not usable because of the
  obscuring wing of the aircraft.
    Nadeu draw a thin, red line rough from where the horizon meets the left
  edge of the image to where the horizon disappears behind the wing.
    Zooming in, one can see that the horizon is slightly curved in the image
  (see Figure~\ref{fig:horizon-35k-markup} and
  Figure~\ref{fig:horizon-35k-markup-zoom}).
    The visible horizon spans about 3100~pixels.}
  \label{fig:horizon-35k}
\end{figure}

\newcommand{\camurl}{https://www.camerafv5.com/devices/manufacturers/samsung/sm-g930w8_heroltebmc_0}

Sébastien Nadeau took the image in Figure~\ref{fig:horizon-35k}.
He
\href%
{https://qr.ae/pKll62}%
{described his observation in a post at {\it Quora}.}%
\footnote{\url{https://qr.ae/pKll62}}
%
Nadeau indicated that he captured an image of the horizon at about 35,000~ft
in altitude.

Nadeau mentioned that the camera corresponds to a Samsung Galaxy S7.
%
Many a different kind of camera was installed in the various versions of the
S7.
%
I downloaded and saved his image to a file
\begin{lstlisting}
horizon-35k.png
\end{lstlisting}
and then at the \texttt{bash}-prompt under GNU/Linux, I ran ran the command,
\begin{lstlisting}
strings horizon-35.png
\end{lstlisting}
in order to extract the ASCII-strings from the image-file.
%
Although Nadeau, in a paint-program, manually drew a straight, red line onto
the image just under the horizon, in order to demonstrate its curvature, his
saving the image from the paint-program seems not to have eliminated the
source of the image in the PNG-file.
%
One of the first strings emitted from the command that I ran above is
\begin{lstlisting}
SM-G930W8
\end{lstlisting}
which happens to be one of the camera-model identifiers for the S7.
%
So Nadeau's camera's model seems to be \href{\camurl}{SM-G930W8}.%
\footnote{%
  {\scriptsize \url{\camurl}}\\

  \begin{tabular}{lrr}
    horizontal & 67.8~deg & 4032 pixels\\
    vertical   & 55.7~deg & 3024 pixels
  \end{tabular}%
}
I calculate, by means of the model-identifier, that the focal-length $f$ of
the camera in pixels obeys the following constraint:
\begin{equation}
  f \tan\frac{67.8 \; \text{deg}}{2} = \frac{4032}{2} \; \text{pixels}.
\end{equation}
%
So, we have $f = 3000$~pixels.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{horizon-35k-markup}
  \end{center}
  \caption{%
    Central region from Figure~\ref{fig:horizon-35k} with some overlaid
    markings.
    %
    The center of the image is marked as point $K$.
    %
    Centered on $K$ are two green circles.
    %
    (1) The smaller, green circle's radius is the distance from $K$ to the
    nearest point $P$ on the horizon.
    %
    (2) The larger, green circle's radius is the largest radius from $K$ such
    that the circle intersects the horizon at points $L$ and $R$ that are
    both visible in the image.
    %
    The yellow line is the chord from $L$ to $R$ with midpoint $M.$
    %
    The angular separation between $P$ and $M$ is a measure of the horizon's
    curvature.
    %
    The black line at the top indicates where the horizon would be if the
    Earth were flat and had large extent.
    %
    The red line at the bottom is Nadeau's hand-drawn line roughly indicating
    the horizon's curvature.%
  }
  \label{fig:horizon-35k-markup}
\end{figure}

To calculate the predicted value for $\varepsilon$ one must estimate
$\varphi.$
%
In Figure~\ref{fig:horizon-35k-markup}, the length of the line-segment $LR$
is 2130~pixels.
%
\begin{equation}
  \varphi \simeq 2 \arctan \frac{2130}{2 \times 3000},
\end{equation}
and so $\varphi \simeq 39.1$~deg.
%
The approximation here is good to about one part in a thousand.
%
There is some round-off error at about one part in a thousand, and there is
some smaller error in ignoring that the horizon does not pass right through
the center of the image (though it passes within 20~pixels or so) and in
ignoring that the horizon is not exactly parallel with a pixel-row (though
the angular deviation from row-parallelism is tiny).

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{curvature}
  \end{center}
  \caption{
    Predicted value of $\varepsilon$ as a function of altitude~$h$ and
    angular baseline $\varphi$.
    %
    The right edge of the plot corresponds to $h = 35,000$~ft.%
  }
  \label{fig:predicted}
\end{figure}

For the value of $\varphi$ extracted from the image, the predicted value of
$\varepsilon$ is about 0.00354~radian (0.203~degree) at $h=$35,000~ft.
%
(See Figure~\ref{fig:predicted} for a broad overview of what would be
predicted at different altitudes and for different values of $\varphi,$ which
is limited by the camera's field of view.)
%
Because the measured distance from $P$ to $M$ is close to the center of the
image, one can use $f \tan\varepsilon$ as a very good approximation of the
number of pixels.
%
The predicted measure of curvature in this case is 10.6~pixels.
%
The observed measure of curvature is about 10~pixels.
%
To determine the observed value of $\varepsilon,$ I imported Nadeau's image
into LibreOffice Draw.
%
I scaled the page exactly to fit the image, so that each pixel is 10~mm.
%
Then I drew the circles, lines, and text that can be seen in
Figure~\ref{fig:horizon-35k-markup} and in
Figure~\ref{fig:horizon-35k-markup-zoom}.
%
I was able to do all of this at high enough magnification so that the
measurement should have an error of no more than pixel or so.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{horizon-35k-markup-zoom}
  \end{center}
  \caption{%
    Magnified view of central portion of Figure~\ref{fig:horizon-35k-markup}.
    %
    The center of the image is marked as point $K$.
    %
    Centered on $K$ is a green circle, whose radius is the distance from $K$
    to the nearest point $P$ on the horizon.
    %
    The yellow line is the central portion of the chord from $L$ to $R$ in
    Figure~\ref{fig:horizon-35k-markup} with midpoint $M$.
    %
    The angular separation between $P$ and $M$ is a measure of the horizon's
    curvature.
    %
    The black line at the top indicates where the horizon would be if the
    Earth were flat and had large extent.
    %
    The red line at the bottom is Nadeau's hand-drawn line roughly indicating
    the horizon's curvature.%
    }
  \label{fig:horizon-35k-markup-zoom}
\end{figure}

\section{Conclusion}

The predicted value of $\varepsilon$ is 10.6~pixels, and the measured value
is about 10~pixels.
%
The curvature of the horizon at 35,000~ft appears, from Nadeau's image taken
with his cell-phone camera, to be consistent with what is predicted on the
basis of a perfectly spherical model of the Earth with no atmosphere.
%
The simplest interpretation is that, for this particular measurement, the
effects of the atmosphere and the Earth's asphericity are not large enough to
be noticeable.

\end{document}

