---
author: Thomas E. Vaughan
description: What a spherical model predicts about apparent curvature.
image: https://tevaughan.gitlab.io/articles/horizon/horizon-35k-markup-zoom.png
title: The Horizon of a Spherical Earth
url: https://tevaughan.gitlab.io/articles/horizon/index.html
---

- [Up](../index.md).

Does a spherical model of the Earth predict what one sees with a cell-phone
camera from 35,000 ft?  Let's test it!

What does the model predict?  A perfect sphere with radius $r = 3960$ mi and
with no surrounding atmosphere predicts that a cell-phone camera, with focal
length of 3000 pixels and at an altitude of 35,000 ft, will see 10.6 pixels
(0.2 deg) of curvature in the horizon over an angular baseline of 39.1 deg.
The horizon is predicted to be 3.31 deg farther from the zenith than the
horizon of a flat-Earth model of large extent.

What does one actually see?  The measured curvature is about 10 pixels,
consistent with the prediction of 10.6 pixels.

![
The center of the image is marked as point K.
Centered on K is a green circle, whose radius is the distance from K to the
nearest point P on the horizon.
The yellow line is the central portion of the chord connecting the two most
widely spaced points on the horizon such that both are same distance from K,
and M is the midpoint of the chord.
The angular separation between P and M is a measure of the horizon's
curvature.
The black line at the top indicates where the horizon would be if the Earth
were flat and had large extent.
The angle between P and M is about 0.2 deg at 35,000 ft and can be determined
by image (like this one) from a typical cell-phone
camera.
](horizon-35k-markup-zoom.png)

- [HTML](article.html), [PDF](article.pdf)

<!-- EOF -->
