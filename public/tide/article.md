
---
title: Tidal Force
author: Thomas E. Vaughan
abstract: |
  The tidal force is sometimes misunderstood, especially by folk, like
  flat-Earth proponents, who seem to misunderstand in order to argue against
  the standard view of tides.
...

- [Up](../index.md).

## Introduction

Here is the misconception as a meme that I found on
[X (formerly Twitter)](https://x.com):

![Misconception of tidal force.](tide-misconception.png)

That was posted by a proponent of the flat Earth.
Such a proponent attempts to show that one or another prediction of the
standard model of the Earth, the Earth-Moon system, etc., does not explain
what is observed.
Usually, in my experience, the attempt involves some misconception of the
standard model.
Yet the misconception is presented as though it were a plausible reading of
the standard model.

In the present case, the lower part of the figure includes part of the idea
predicted by Newtonian gravity but leaves out a critical piece.
In the Earth, each point nearer the Moon is pulled more strongly by the
Moon's gravity than is a point farther from the Moon.
The length of each arrow might be taken as an indication of the strength of
the gravitational acceleration from the opposite body.

Yet the distortion of the ocean is not depicted as the Newtonian model
predicts.
(Nor is there any depicted distortion of the lithosphere, which does distort,
though not so much as the ocean.)
The main problems with the depiction are,

1. that it does not show the Moon-force vectors all the way around the Earth
   and

2. that it does not show the Moon-force vectors as they would appear in the
   rest-frame of the center of the Earth.

As a body, the Earth in the Newtonian model feels both a compression toward
the Earth's center along the line perpendicular to the Moon and a stretching
away from the center of the Earth along the line toward the Moon.
Because the Earth is approximately a rigid body in this context, the relevant
frame for analyzing distortion is the frame attached to the Earth's center.

## How Tidal Force Works

In the Newtonian gravitational model with gravitational constant $G$, the
acceleration

\begin{equation}
  a = \frac{Gm}{r^2},
\end{equation}

of a low-mass test-point due to a spherically symmetric mass $m$ is
proportional to the inverse square of the distance $r$ between the center of
the mass and the test-point.

Consider three low-mass test-points, a, o, and b, along the same line with
the mass, and let the respective distances from the mass be

\begin{eqnarray}
  r_\text{a} &=& r - \rho\\
  r_\text{o} &=& r\\
  r_\text{b} &=& r + \rho.
\end{eqnarray}

Consider two more low-mass test-points, c and d, each at distance $\rho$ from
o and each, along with o, on the line perpendicular to the line between o and
the mass.
Suppose that, at a given moment in time, all of the points are moving at the
same velocity; that is, each of them is at rest in the frame attached to o.
The problem is to determine the apparent acceleration for each of test-points
a, b, c, and d in the non-inertial frame attached to o.

\begin{eqnarray}
  \vec{a}_\text{ao} &=& \vec{a}_\text{a} - \vec{a}_\text{o}\\
  \vec{a}_\text{bo} &=& \vec{a}_\text{b} - \vec{a}_\text{o}\\
  \vec{a}_\text{co} &=& \vec{a}_\text{c} - \vec{a}_\text{o}\\
  \vec{a}_\text{do} &=& \vec{a}_\text{d} - \vec{a}_\text{o}.
\end{eqnarray}

