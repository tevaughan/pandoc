@echo off

rem  On MS-Windows, the following must be installed in order to build:
rem
rem  - cmake
rem  - gnuplot
rem  - imagemagick (needed by latexml)
rem  - latexml (via `cpan -T LaTeXML` after installing perl)
rem  - make
rem  - pandoc
rem  - perl (Strawberry, needed by latexml)
rem  - pdf2svg
rem  - texlive (needed by latexml)

rem  Directory of this bat-file is top of source-tree.
set "srcDir=%~dp0"

rem  Remove trailing back-slash.
set "srcDir=%srcDir:~0,-1%"

rem  Set build-directory.  All products are under 'public', but so is
rem  corresponding source, and so this is an in-tree-build configuration.
set "bldDir=%srcDir%"

cmake -B "%bldDir%" -S "%srcDir%" -G "Unix Makefiles"
echo "Now type 'make'."

rem EOF
