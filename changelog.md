
# History of Changes

## Version 0.2 (2024 Jan 20)

- Stop building documents in `.gitlab-ci.yml`.
  - Rather, include build-documents directly under public in git via LFS.

- Now provide clean-target for each output of a custom command in any
  `CMakeListst.txt`.

- Fix figure in Horizon to provide wider angular baselines and to have
  epsilon on y-axis-label.

- Add this `changelog.md`.

## Version 0.1 (2024 Jan 19)

- First decent version of "The Horizon of a Spherical Earth."

