-- The name of this file is passed (by way of CMakeLists.txt) to the
-- '--lua-filter' option on pandoc's command line.

-- If link point to tex-file, change link to point to corresponding
-- html-file.
function Link(el)
	el.target = string.gsub(el.target, "%.tex", ".html")
	return el
end

-- EOF
