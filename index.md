---
title: "Top of Source-Tree for Articles-Project"
author: "Thomas E. Vaughan"
---

- [Top of source-tree for static website.](public/)

## Pandoc-Markdown

The occasional thing here, like the [top-level index-file] for the static
website, is written in [pandoc-markdown].

- Pandoc-markdown is good for an index-file or a short article.

- Pandoc-markdown works well for an article, so long as it have neither
  a complicated figure nor a large amount of mathematics.

- [Pandoc][pandoc] can understand [LaTeX] mathematics embedded within
  [markdown]-source, but typically equations are not numbered in the rendered
  output.

[top-level index-file]: public/index.md
[pandoc-markdown]: https://pandoc.org/MANUAL.html#pandocs-markdown
[pandoc]: https://pandoc.org
[markdown]: https://www.markdownguide.org
[LaTeX]: https://www.latex-project.org

## LaTeX

If numbered equations or complicated, embedded figures be required, then I
use LaTeX as the source rather than markdown.  In that case, the HTML-version
of the document is generated by way of [latexml/latexmlpost][latexml].

[latexml]: https://math.nist.gov/~BMiller/LaTeXML/

<!-- EOF -->
